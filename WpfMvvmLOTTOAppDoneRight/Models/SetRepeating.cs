﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WpfMvvmLOTTOAppDoneRight.Helpers;
using WpfMvvmLOTTOAppDoneRight.Models;

namespace WpfMvvmLOTTOAppDoneRight.Models
{
     public class SetRepeating : NotificationObject
     {
         #region Ctor
         public SetRepeating( int i )
         {
             DateTimeStemp = SetStorage.SetList[i].Item1;
             NoOfRepeatingNotSortedSets = string.Join(" ", SetStorage.SetList[i].Item2.Select(x => x.ToString("00"))) + " = " + SetStorage.GetNumberOfRepeatsIntArray("NotSorted", i);//.ToString();
             NoOfRepeatingSortedSets = string.Join(" ", SetStorage.SetList[i].Item3.Select(x => x.ToString("00"))) + " = " + SetStorage.GetNumberOfRepeatsIntArray("Sorted", i).ToString();             
             NoOfRepeatingSetsDoubles = string.Join(" ", SetStorage.SetList[i].Item4.DefaultIfEmpty("none")) + " = " + SetStorage.GetNumberOfRepeatsGeneratedStrArray(2, i).ToString();
             NoOfRepeatingSetsTriples = string.Join(" ", SetStorage.SetList[i].Item5.DefaultIfEmpty("none")) + " = " + SetStorage.GetNumberOfRepeatsGeneratedStrArray(3, i).ToString();
             NoOfRepeatingSetsQuadriples = string.Join(" ", SetStorage.SetList[i].Item6.DefaultIfEmpty("none")) + " = " + SetStorage.GetNumberOfRepeatsGeneratedStrArray(4, i).ToString();
             NoOfRepeatingSetsQuituple = string.Join(" ", SetStorage.SetList[i].Item7.DefaultIfEmpty("none")) + " = " + SetStorage.GetNumberOfRepeatsGeneratedStrArray(5, i).ToString();
             NoOfRepeatingSetsSextuple = string.Join(" ", SetStorage.SetList[i].Rest.Item1.DefaultIfEmpty("none")) + " = " + SetStorage.GetNumberOfRepeatsGeneratedStrArray(6, i).ToString();
            
             //Adding each finding to the FamousSetList
             SetFamousRepeats.FamousSetList.Add(new Tuple<DateTime,string,string,string,string,string,string,Tuple<string>>(DateTimeStemp,NoOfRepeatingNotSortedSets,NoOfRepeatingSortedSets,NoOfRepeatingSetsDoubles,NoOfRepeatingSetsTriples,NoOfRepeatingSetsQuadriples,NoOfRepeatingSetsQuituple, new Tuple<string>(NoOfRepeatingSetsSextuple)));
             //SetFamousRepeats.FamousSetList.Add(new Tuple<string[], int>(SetStorage.SetList[i].Item3.Select(x => x.ToString("00")).ToArray(), SetStorage.GetNumberOfRepeatsIntArray("Sorted", i)));
         }
         #endregion

         #region Properties
         private DateTime _dateTimeStemp;

         public DateTime DateTimeStemp
         {
             get { return _dateTimeStemp; }
             set 
             {
                 if (_dateTimeStemp != value)
                 {
                     _dateTimeStemp = value;
                     RaisePropertyChanged(()=> DateTimeStemp);
                 }
             }
         }

         private string _noOfRepeatingNotSortedSets;

         public string NoOfRepeatingNotSortedSets
         {
             get { return _noOfRepeatingNotSortedSets; }
             set 
             {
                 if (_noOfRepeatingNotSortedSets != value)
                 {
                     _noOfRepeatingNotSortedSets = value;
                     RaisePropertyChanged(() => NoOfRepeatingNotSortedSets);
                 }
                 }
         }

         private string _noOfRepeatingSortedSets;

         public string NoOfRepeatingSortedSets
         {
             get { return _noOfRepeatingSortedSets; }
             set
             {
                 if (_noOfRepeatingSortedSets != value)
                 {
                     _noOfRepeatingSortedSets = value;
                     RaisePropertyChanged(()=>NoOfRepeatingSortedSets);
                 }
             }
         }
        
         private string _noOfRepeatingSetsDoubles;

         public string NoOfRepeatingSetsDoubles
         {
             get { return _noOfRepeatingSetsDoubles; }
             set
             {
                 if (_noOfRepeatingSetsDoubles != value)
                 {
                     _noOfRepeatingSetsDoubles = value;
                     RaisePropertyChanged(() => NoOfRepeatingSetsDoubles);
                 }
             }
         }

         private string _noOfRepeatingSetsTriples;

         public string NoOfRepeatingSetsTriples
         {
             get { return _noOfRepeatingSetsTriples; }
             set
             {
                 if (_noOfRepeatingSetsTriples != value)
                 {
                     _noOfRepeatingSetsTriples = value;
                     RaisePropertyChanged(() => NoOfRepeatingSetsTriples);
                 }
             }
         }

         private string _noOfRepeatingSetsQuadriples;

         public string NoOfRepeatingSetsQuadriples
         {
             get { return _noOfRepeatingSetsQuadriples; }
             set
             {
                 if (_noOfRepeatingSetsQuadriples != value)
                 {
                     _noOfRepeatingSetsQuadriples = value;
                     RaisePropertyChanged(() => NoOfRepeatingSetsQuadriples);
                 }
             }
         }
         private string _noOfRepeatingSetsQuituple;

         public string NoOfRepeatingSetsQuituple
         {
             get { return _noOfRepeatingSetsQuituple; }
             set
             {
                 if (_noOfRepeatingSetsQuituple != value)
                 {
                     _noOfRepeatingSetsQuituple = value;
                     RaisePropertyChanged(() => NoOfRepeatingSetsQuituple);
                 }
             }
         }
         private string _noOfRepeatingSetsSextuple;

         public string NoOfRepeatingSetsSextuple
         {
             get { return _noOfRepeatingSetsSextuple; }
             set
             {
                 if (_noOfRepeatingSetsSextuple != value)
                 {
                     _noOfRepeatingSetsSextuple = value;
                     RaisePropertyChanged(() => NoOfRepeatingSetsSextuple);
                 }
             }
         }
         #endregion

         #region MostFamousSetRepeats
         //public static string GetMostFamousSet1()
         //{
         //    return SetStorage.SetList.Select(x => x.Item4.ToString().Split('=').OrderByDescending(y => y));//.ToString().LastOrDefault(y=>y.spl);

         //}
         #endregion
     }
}
