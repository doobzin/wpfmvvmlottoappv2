﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using WpfMvvmLOTTOAppDoneRight.Helpers;

namespace WpfMvvmLOTTOAppDoneRight.Models
{
    #region Ctor
    public class SetStorage: NotificationObject
    {
        public SetStorage(int i, int numToFind)
        {
            NoOfRepeatingSingles = (i + 1).ToString("00") + " = " + GetNumberOfRepeatsPer(numToFind).ToString();
            ///
        }
   #endregion

        #region Propeties
      
        #region SetStorageCollection Properties

        private string _noOfRepeatingSingles;

        public string NoOfRepeatingSingles
        {
            get { return _noOfRepeatingSingles; }
            set
            {
                if (_noOfRepeatingSingles != value)
                {
                    _noOfRepeatingSingles = value;
                    RaisePropertyChanged(() => NoOfRepeatingSingles);
                }
            }
        }
        #endregion

        #region SetRepeatingCollection Properties
        private string _noOfRepeatingSetsDoubles;

        public string NoOfRepeatingSetsDoubles
        {
            get { return _noOfRepeatingSetsDoubles; }
            set
            {
                if (_noOfRepeatingSetsDoubles != value)
                {
                    _noOfRepeatingSetsDoubles = value;
                    RaisePropertyChanged(() => NoOfRepeatingSetsDoubles);
                }
            }
        }

        private string _noOfRepeatingSetsTriples;

        public string NoOfRepeatingSetsTriples
        {
            get { return _noOfRepeatingSetsTriples; }
            set
            {
                if (_noOfRepeatingSetsTriples != value)
                {
                    _noOfRepeatingSetsTriples = value;
                    RaisePropertyChanged(() => NoOfRepeatingSetsTriples);
                }
            }
        }
        #endregion
        #endregion
        /// <summary>
        /// this
        /// </summary>
        public static List<Tuple<DateTime, int[], int[], string[], string[], string[], string[], Tuple<string[]>>> SetList = new List<Tuple<DateTime, int[], int[], string[], string[], string[], string[], Tuple<string[]>>>();


        public static int GetNumberOfRepeatsIntArray(string setType, int i)
        {
            int count = 0;
            switch (setType)
            {
                case "NotSorted" :
                    count = SetList.Count(x => x.Item2.SequenceEqual(SetList[i].Item2)) - 1;//Minus 1 because it counts itself as it is comparing itself to all in list 

                    break;
                case "Sorted" :
                    count = SetList.Count(x => x.Item3.SequenceEqual(SetList[i].Item3)) - 1;//Minus 1 because it counts itself as it is comparing itself to all in list 
                    break;
            }

            return count; 
            
        }

        public static int GetNumberOfRepeatsGeneratedStrArray(int setOf, int i)
        {
            
            int count = 0;
            string[] strSet = { };
            switch (setOf)
            {
                case 2:
                    strSet = SetList[i].Item4;

                    //count = SetList.Select(x => x.Item4).ToArray().Count(x => x.SequenceEqual(strSet)) - 1; //Minus 1 because it counts itself as it is comparing itself to all in list 
                    count = SetList.Count(x => x.Item4.SequenceEqual(strSet)) - 1; //Minus 1 because it counts itself as it is comparing itself to all in list 
                    
                    break;
                case 3:
                    strSet = SetList[i].Item5;

                    count = SetList.Select(x => x.Item5).ToArray().Count(x => x.SequenceEqual(strSet)) - 1;
                    break;
                case 4:
                    strSet = SetList[i].Item6;

                    count = SetList.Select(x => x.Item6).ToArray().Count(x => x.SequenceEqual(strSet)) - 1;
                    break;
                case 5:
                    strSet = SetList[i].Item7;

                    count = SetList.Select(x => x.Item7).ToArray().Count(x => x.SequenceEqual(strSet)) - 1;
                    break;
                case 6:
                    strSet = SetList[i].Rest.Item1;

                    count = SetList.Select(x => x.Rest.Item1).ToArray().Count(x => x.SequenceEqual(strSet)) -1;

                    break;
            }
            return count;
         }

        public static int GetNumberOfRepeatsPer(int numberToFind)
        {
            return SetList.Select(x => x.Item3).ToArray().Count(x => x.Contains(numberToFind)); //How many one's in each set... for example.
        }
    }
}
