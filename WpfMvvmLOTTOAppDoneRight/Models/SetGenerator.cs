﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows; 
using System.Windows.Media;
using WpfMvvmLOTTOAppDoneRight.Helpers;
using WpfMvvmLOTTOAppDoneRight.ViewModels;

namespace WpfMvvmLOTTOAppDoneRight.Models
{
    public class SetGenerator : NotificationObject
    {
        #region Ctor
        public SetGenerator(int[] notSortedSet, DateTime timeStemp)
        {
            //Note : the Object should sort all this on it self EG: getSecuenceOf(Doubles)... as methods in it.

            NotSortedSet = ToString(notSortedSet.Select(x => x.ToString("00")).ToArray());
            TimeStemp = timeStemp;
            SortedSet = ToString(notSortedSet.OrderBy(x => x).Select(x => x.ToString("00")).ToArray());
            GetSequentialSetOf2 = ToString(GetSequentialSetOf(2, notSortedSet));
            GetSequentialSetOf3 = ToString(GetSequentialSetOf(3, notSortedSet));
            GetSequentialSetOf4 = ToString(GetSequentialSetOf(4, notSortedSet));
            GetSequentialSetOf5 = ToString(GetSequentialSetOf(5, notSortedSet));
            GetSequentialSetOf6 = ToString(GetSequentialSetOf(6, notSortedSet));
            
            //Adding all sets to SetList Tuple 
            SetStorage.SetList.Add(new Tuple<DateTime, int[], int[], string[], string[], string[], string[], Tuple<string[]>>(DateTime.Now, notSortedSet, notSortedSet.OrderBy(x => x).ToArray(), GetSequentialSetOf(2, notSortedSet), GetSequentialSetOf(3, notSortedSet), GetSequentialSetOf(4, notSortedSet), GetSequentialSetOf(5, notSortedSet),new Tuple<string[]>( GetSequentialSetOf(6, notSortedSet))));
            
        }
        #endregion

        #region ToString()
        public string ToString(int[] intArray)
        {
           return string.Join(" ", intArray);
        }
        public string ToString(string[] intArray)
        {
            return string.Join(" ", intArray);
        }
        public string ToString(DateTime dateTime)
        {
            return dateTime.ToString("yyyy/MM/dd HH:mm:ss");
        }
        #endregion

        #region Properties

        public static string TimeStempNow;

        private string _notSortedSet;
        public string NotSortedSet
        {
            get { return _notSortedSet; }
            set
            {
                if (_notSortedSet != value)
                {
                    _notSortedSet = value;
                    RaisePropertyChanged(() => NotSortedSet);
                }
            }
        }
        private  DateTime _timeStemp;

        public  DateTime TimeStemp
        {
            get { return _timeStemp; }
            set 
            {
                if (_timeStemp != value)
                {
                    _timeStemp = value;
                    RaisePropertyChanged(() => TimeStemp);
                }
            }
        }


        private string _SortedSet;
        public string SortedSet
        {
            get { return _SortedSet; }
            set
            {
                if (_SortedSet != value)
                {
                    _SortedSet = value;
                    RaisePropertyChanged(() => SortedSet);
                } 
            }
        }

        private string _setSequentialSetOf2;

        public  string GetSequentialSetOf2
        {
            get { return _setSequentialSetOf2; }
            set { _setSequentialSetOf2 = value; }
        }

        private string _getSequentialSetOf3;

        public string GetSequentialSetOf3
        {
            get { return _getSequentialSetOf3; }
            set { _getSequentialSetOf3 = value; }
        }

        private string _getSequentialSetOf4;

        public string GetSequentialSetOf4
        {
            get { return _getSequentialSetOf4; }
            set { _getSequentialSetOf4 = value; }
        }

        private string _getSequentialSetOf5;

        public string GetSequentialSetOf5
        {
            get { return _getSequentialSetOf5; }
            set { _getSequentialSetOf5 = value; }
        }

        private string _getSequentialSetOf6;

        public string GetSequentialSetOf6
        {
            get { return _getSequentialSetOf6; }
            set { _getSequentialSetOf6 = value; }
        }

        #endregion

        #region Set Generator Core

        #region List Help on draw (All Lotto Numbers)
        public static List<int> AllLottoNoList;

        public static List<int> LottoTempNoList;

        public static List<int> Get(int selectedBall)
        {
            return AllLottoNoList.Where(a => a == Convert.ToInt32(selectedBall)).ToList();
        }
        public static void TakeOut(int selectedBall)
        {
            LottoTempNoList = LottoTempNoList.Except(Get(selectedBall)).ToList();
        }
        #endregion

        public static readonly Random randomSeed = new Random();

        #region Random Ball Selector
        public static int SelectBallForm(List<int> RemainingBalls)
        {
            return RemainingBalls.ElementAt(randomSeed.Next(RemainingBalls.Count()));
        }
        #endregion

        #region Random Genarate Set From List
        

        public static int[] RandomGenarateSet()
        {
            AllLottoNoList = new List<int> { 01, 02, 03, 04, 05, 06, 07, 08, 09, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49 };
            LottoTempNoList = AllLottoNoList;

            int[] tempHolder = new int[6];

            for (int i = 0; i < 6; i++)
            {
                int selectedBall = SelectBallForm(LottoTempNoList);
                TakeOut(selectedBall);
                tempHolder[i] = selectedBall;
                // MessageBox.Show(string.Format("This is the random number: {0} and the ball count: {1}",selectedBall.ToString("00"), LottoTempNoList.Count));tempHolder[i] = selectedBall;
            }
            return  tempHolder;
        }

       

        #endregion

        #endregion

        #region Get Set Consecutive Numbers

        #region Generate new array From set SkipTake()

        public static int[] SkipTake(int[] set, int skip, int take)
        {
            return set.Skip(skip).Take(take).ToArray();

        }


        #endregion

        #region Booleans IsSequential()
        public static bool IsSequential(int[] array)
        {
            return array.Zip(array.Skip(1), (a, b) => (a + 1) == b).All(x => x);
        }
        #endregion

        #region GetSequentialSets

        #region GetSequentialSets
        public static string[] GetSequentialSetOf(int take, int[] notSortedSet)
        {
            string[] answer = { };
            int[] SortedSet = notSortedSet.OrderBy(x => x).ToArray(); //sorts the set accending order
           
            for (int skip = 0; skip < 7 - take; skip++)
            {
                int[] newSet = SkipTake(SortedSet, skip, take);
                if (IsSequential(newSet) == true)
                {
                    if (answer.Length == 0)
                    {
                        answer = newSet.Select(x => x.ToString("00")).ToArray();
                    }
                    else
                    {
                        answer = answer.Concat(new[] { "-" }).ToArray();
                        answer = answer.Concat(newSet.Select(x => x.ToString("00"))).ToArray();

                    }
                   // MessageBox.Show(string.Format("This is the results mmmmmm: {0}", string.Join(" ", answer)));
                }


            }
            //MessageBox.Show(string.Format("This is the finallllll results: {0}", string.Join(" ", answer)));

            return answer;
        }
        #endregion

        #endregion

        #endregion
    }
}
