﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Documents;
using WpfMvvmLOTTOAppDoneRight.Helpers;

namespace WpfMvvmLOTTOAppDoneRight.Models
{
    public class SetFamousRepeats : NotificationObject
    {
        #region Ctor
        public SetFamousRepeats(string item)
        {
            MostFamousDoubles = item;
               
        }
        #endregion

        #region Properties
        private string _mostFamousDoubles;

        public string MostFamousDoubles
        {
            get { return _mostFamousDoubles; }
            set 
            {
                if (_mostFamousDoubles != value)
                {
                    _mostFamousDoubles = value;
                    RaisePropertyChanged(()=> MostFamousDoubles);
                }
            }
        }
        
        #endregion

        public static List<Tuple<DateTime, string, string, string, string, string, string, Tuple<string>>> FamousSetList = new List<Tuple<DateTime, string, string, string, string, string, string, Tuple<string>>>();

        public static List<string> GetFamousSetRepeat(string inSetOf, int take)
        {
            List<string>  mostFamousSet =new List<string> {}; 
            switch (inSetOf)
            {
                case "NotSortedSet":
                    mostFamousSet = FamousSetList.Select(x => x.Item2).ToList().OrderByDescending(x => int.Parse(x.Substring(x.LastIndexOf("=") + 1))).ToList().Distinct().Take(take).ToList();
                    break;
                case "SortedSet":
                    mostFamousSet = FamousSetList.Select(x => x.Item3).ToList().OrderByDescending(x => int.Parse(x.Substring(x.LastIndexOf("=") + 1))).ToList().Distinct().Take(take).ToList();
                    break;
                case "Doubles": 
                    //List<string> list = FamousSetList.Select(x => x.Item3).ToList();
                    //List<string> decList = list.OrderByDescending(x => int.Parse(x.Substring(x.LastIndexOf("=") + 1))).ToList();
                    //List<string> actual = decList.Distinct().Take(take).ToList();
                    mostFamousSet = FamousSetList.Select(x => x.Item4).ToList().OrderByDescending(x => int.Parse(x.Substring(x.LastIndexOf("=") + 1))).ToList().Distinct().Take(take).ToList();
                    break;

                case "Triples":
                    mostFamousSet = FamousSetList.Select(x => x.Item5).ToList().OrderByDescending(x => int.Parse(x.Substring(x.LastIndexOf("=") + 1))).ToList().Distinct().Take(take).ToList();
                    break;

                case "Quadriples":
                    mostFamousSet = FamousSetList.Select(x => x.Item6).ToList().OrderByDescending(x => int.Parse(x.Substring(x.LastIndexOf("=") + 1))).ToList().Distinct().Take(take).ToList();
                    break;
                case "Quituple":
                    mostFamousSet = FamousSetList.Select(x => x.Item7).ToList().OrderByDescending(x => int.Parse(x.Substring(x.LastIndexOf("=") + 1))).ToList().Distinct().Take(take).ToList();
                    break;
                case "Sextuples":
                    mostFamousSet = FamousSetList.Select(x => x.Rest.Item1).ToList().OrderByDescending(x => int.Parse(x.Substring(x.LastIndexOf("=") + 1))).ToList().Distinct().Take(take).ToList();
                    break;
            }
            return mostFamousSet;
        }
    }
}
