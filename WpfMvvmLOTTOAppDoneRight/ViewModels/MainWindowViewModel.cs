using System;
using System.Collections.ObjectModel;
using System.Windows.Input;
using System.Windows;
using WpfMvvmLOTTOAppDoneRight.Helpers;
using WpfMvvmLOTTOAppDoneRight.Models;

namespace WpfMvvmLOTTOAppDoneRight.ViewModels
{
    public class MainWindowViewModel : BaseViewModel
    {
        #region Properties
        //for ProgressBar
        #region ProgressBar
        private Int32 _workDone;

        public Int32 WorkDone
        {
            get { return _workDone; }
            set
            {
                if (_workDone != value)
                {
                    _workDone = value;
                    RaisePropertyChanged(()=> WorkDone);
                }
            }
        }

        private Int32 _workTotal;

        public Int32 WorkTotal
        {
            get { return _workTotal; }
            set
            {
                if (_workTotal != value)
                {
                    _workTotal = value;
                    RaisePropertyChanged(()=> WorkTotal);
                }
            }
        }
       
        
        #endregion
        //for SetGenerater
        #region NoOfSets

        private int _txtNoOfSets;
        public int txtNoOfSets
        {
            get { return _txtNoOfSets; }
            set
            {
                if (_txtNoOfSets != value)
                {
                    _txtNoOfSets = value;
                    RaisePropertyChanged(() => txtNoOfSets);
                }
            }
        }

        #endregion
        //for SetRepeating
        #region NumRepeates
        private int _txtNumRepeates;

        public int txtNumRepeates
        {
            get { return _txtNumRepeates; }
            set
            {
                if (_txtNumRepeates != value)
                {
                    _txtNumRepeates = value;
                    RaisePropertyChanged(() => txtNumRepeates);
                }
            }
        }

        private string[] _setToFindStrArray;

        public string[] SetToFindStrArray
        {
            get { return _setToFindStrArray; }
            set 
            {
                if (_setToFindStrArray != value)
                {
                    _setToFindStrArray = value;
                    RaisePropertyChanged(()=> SetToFindStrArray);
                }
            }
        }

        private int[] _setToFindIntArray;

        public int[] SetToFindIntArray
        {
            get { return _setToFindIntArray; }
            set 
            {
                if (_setToFindIntArray != value)
                {
                    _setToFindIntArray = value;
                    RaisePropertyChanged(()=> SetToFindIntArray);
                }
            }
        }
        
        #endregion

        #region Combobox

        private string _cbItem;

        public string cbItem
        {
            get { return _cbItem; }
            set 
            {
                if (_cbItem != value)
                {
                    _cbItem = value;
                    RaisePropertyChanged(()=>cbItem);
                   // MessageBox.Show(string.Format("This is the selected item: {0}",cbItem.Remove(0,38)));
                }

            }
        }
        
        
        #endregion

        #region SetGeneratorCollection

        private ObservableCollection<SetGenerator> _setGeneratorCollection;
        public ObservableCollection<SetGenerator> SetGeneratorCollection
        {
            get { return _setGeneratorCollection; }
            set
            {
                if (_setGeneratorCollection != value)
                {
                    _setGeneratorCollection = value;
                    RaisePropertyChanged(() => SetGeneratorCollection);
                }
            }
        }

        #endregion

        #region SetStorageCollection

        private ObservableCollection<SetStorage> _setStorageCollection;
        public ObservableCollection<SetStorage> SetStorageCollection
        {
            get { return _setStorageCollection; }
            set
            {
                if (_setStorageCollection != value)
                {
                    _setStorageCollection = value;
                    RaisePropertyChanged(() => SetStorageCollection);
                }
            }
        }

        #endregion

        #region SetRepeatingCollection

        private ObservableCollection<SetRepeating> _setRepeatingCollection;
        public ObservableCollection<SetRepeating> SetRepeatingCollection
        {
            get { return _setRepeatingCollection; }
            set
            {
                if (_setRepeatingCollection != value)
                {
                    _setRepeatingCollection = value;
                    RaisePropertyChanged(() => SetRepeatingCollection);
                }
            }
        }

        #endregion

        #region FamousSetRepeatsCollection

        private ObservableCollection<SetFamousRepeats> _famousSetRepeatsCollection;
        public ObservableCollection<SetFamousRepeats> FamousSetRepeatsCollection
        {
            get { return _famousSetRepeatsCollection; }
            set
            {
                if (_famousSetRepeatsCollection != value)
                {
                    _famousSetRepeatsCollection = value;
                    RaisePropertyChanged(() => FamousSetRepeatsCollection);
                }
            }
        }

        #endregion

        #endregion

        #region Commands

        public ICommand GenerateDataCommand { get { return new DelegateCommand(OnGenerateData, CanExecuteGenerateData); } }
        public ICommand GetFamousCommand { get { return new DelegateCommand(OnGetFamous, CanExecuteGetFamous); } }

        
        #endregion

        #region Ctor
        public MainWindowViewModel()
        {
           // WorkTotal = 10;
           GenerateData(10);
           SetFamousRepeats.FamousSetList.Clear(); //Refreshes the list so that it will not repeat the same items in a list
           SetRepeats();
           FamousSetRepeating("Doubles",10);
        }
        #endregion

        #region Command Handlers


        private void OnGenerateData()
        {
            WorkDone = 0;
            SetFamousRepeats.FamousSetList.Clear(); //Refreshes the list so that it will not repeat the same items in a list
            WorkTotal = txtNoOfSets;
            GenerateData(txtNoOfSets);
            SetRepeats();
            FamousSetRepeating("Doubles", 10);
        }

        private bool CanExecuteGenerateData()
        {
            if (txtNoOfSets != 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        private bool CanExecuteStoreData()
        {
            return true;
        }
        //
        private void OnGetFamous()
        {
            FamousSetRepeating(cbItem.Remove(0, 38), txtNumRepeates);
        }
        private bool CanExecuteGetFamous()
        {
            if (txtNumRepeates != 0 & cbItem != null)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        #endregion

        private void GenerateData(int NoOfSets)
        {
            WorkTotal = NoOfSets;
            SetGeneratorCollection = new ObservableCollection<SetGenerator>();
            for (var i = 0; i < NoOfSets; i++)
            {
                SetGeneratorCollection.Add(new SetGenerator(
                    SetGenerator.RandomGenarateSet(),
                     DateTime.Now
                    ));
                WorkDone = i+2;

            }
            //
            SetStorageCollection = new ObservableCollection<SetStorage>();
            for (var i = 0; i < 49; i++)
            {

                SetStorageCollection.Add(new SetStorage(
                    i, i + 1
                ));
            }
        }
       
        private void SetRepeats()
        {
            SetRepeatingCollection = new ObservableCollection<SetRepeating>();
            for (var i = 0; i < SetStorage.SetList.Count; i++)
            {

                SetRepeatingCollection.Add(new SetRepeating(
                    i
                ));
            }
        }
       
        private void FamousSetRepeating(string setOf, int take)
        {
            FamousSetRepeatsCollection = new ObservableCollection<SetFamousRepeats>();
           
            foreach (var item in SetFamousRepeats.GetFamousSetRepeat(setOf,take))
            {
                FamousSetRepeatsCollection.Add(new SetFamousRepeats(item));
            }
        }
    }
}