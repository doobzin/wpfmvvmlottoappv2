﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using WpfMvvmLOTTOAppDoneRight.Models;
using System.Collections.Generic;
using System.Linq;

namespace UnitTestProjectLotto
{
    [TestClass]
    public class UnitTestModels
    {
        [TestMethod]
        public void GetSequentialSetOf()
        {
            //Arrange
            int[] set = { 01, 02, 03, 04, 25, 26 };
            string[] expected = { "01", "02", "-", "02", "03", "-", "03", "04", "-", "25", "26" };

            //Act
            string[] actual = SetGenerator.GetSequentialSetOf(2, set);

            //Assert
            for (int i = 0; i < actual.Length; i++)
            {
                Assert.AreEqual(expected[i], actual[i]);
            }

        }

        [TestMethod]
        public void AddNumberOfRepeats()
        {
            //Arrange
            int[] Set1 = { 01, 02, 03, 04, 25, 26 };
            int[] Set2 = { 11, 20, 30, 40, 45, 46 };
            int[] Set3 = { 11, 12, 13, 40, 45, 46 };
           // int[] NewSet4 = { 11, 20, 30, 40, 45, 46 };
            int[] NewSet4 = { 11, 12, 13, 40, 45, 46 };

            List<Tuple< string, int[], int>> SetList = new List<Tuple<string, int[],int>>();
            SetList.Add(Tuple.Create("2017/06/19",Set1, 1));
            SetList.Add(Tuple.Create("2017/06/19", Set2, 1));
            SetList.Add(Tuple.Create("2017/06/19", Set3, 1));



            //int[] expected = Set3;
            bool expected = true;

            //Act
            bool actual = SetList.Any(x => x.Item2.SequenceEqual(NewSet4)); 
            //int[] actual = {};
            //foreach (var set in SetList)
            //{
            //    if (set.Item2.SequenceEqual(NewSet4))
            //    {
            //        actual = set.Item2.Select(x => x).ToArray();
            //    }
                
            //}
            

            //Assert
            //for (int i = 0; i < SetList.Count; i++)
            //{
            //    Assert.AreEqual(expected[i], actual[i]);

            //}

            Assert.AreEqual(expected, actual);

        }
        [TestMethod]
        public void GetNumberOfRepeatsPer()
        {  
            //Arrange
            int[] Set1 = { 01, 02, 03, 04, 25, 26 };
            int[] Set2 = { 11, 20, 30, 40, 45, 46 };
            int[] Set3 = { 11, 12, 13, 40, 45, 46 };
            int[] Set4 = { 11, 20, 30, 40, 45, 46 };
            int[] Set5 = { 11, 12, 13, 40, 45, 46 };

            List<Tuple<string, int[], int[]>> SetList = new List<Tuple<string, int[], int[]>>();
            SetList.Add(Tuple.Create("2017/06/19", Set1, Set1));
            SetList.Add(Tuple.Create("2017/06/19", Set2, Set2));
            SetList.Add(Tuple.Create("2017/06/19", Set3, Set3));
            SetList.Add(Tuple.Create("2017/06/19", Set4, Set4));
            SetList.Add(Tuple.Create("2017/06/19", Set5, Set5));

            int expected = 2;

            //Act

            int actual = SetList.Select(x => x.Item3).ToArray().Count(x => x.Contains(20));

            //Assert
            
            Assert.AreEqual(expected, actual);
            

        }
         [TestMethod]
        public void GetNumberOfRepeatsIntArray()
        {
            //Arrange
            int[] Set1 = { 01, 02, 03, 04, 25, 26 };
            int[] Set2 = { 11, 20, 30, 40, 45, 46 };
            int[] Set3 = { 11, 12, 13, 40, 45, 46 };
            int[] Set4 = { 11, 20, 30, 40, 45, 46 };
            int[] Set5 = { 11, 12, 13, 40, 45, 46 };

            List<Tuple<string, int[], int[]>> SetList = new List<Tuple<string, int[], int[]>>();
            SetList.Add(Tuple.Create("2017/06/19", Set1, Set1));
            SetList.Add(Tuple.Create("2017/06/19", Set2, Set2));
            SetList.Add(Tuple.Create("2017/06/19", Set3, Set3));
            SetList.Add(Tuple.Create("2017/06/19", Set4, Set4));
            SetList.Add(Tuple.Create("2017/06/19", Set5, Set5));

            int expected = 2;

            //Act

           // int actual = SetList.Select(x => x.Item3).ToArray().Count(x => x.Contains(20));
            int actual = SetList.Count(x => x.Item2.SequenceEqual(Set4));

            //Assert

            Assert.AreEqual(expected, actual);


        }
         [TestMethod]
         public void GetFamousSetRepeat()
         {
             //Arrange
             string Set1 =  "01 02 03 04 25 26 = 0";
             string Set2 =  "11 20 30 40 45 46 = 1";
             string Set3 =  "11 12 13 40 45 46 = 1";
             string Set4 =  "11 20 30 40 45 46 = 1";
             string Set5 =  "11 12 13 40 45 46 = 1";

             List<Tuple<string, string, string>> FamousSetList = new List<Tuple<string, string, string>>();
             FamousSetList.Add(Tuple.Create("2017/06/19", Set1, Set1));
             FamousSetList.Add(Tuple.Create("2017/06/19", Set2, Set2));
             FamousSetList.Add(Tuple.Create("2017/06/19", Set3, Set3));
             FamousSetList.Add(Tuple.Create("2017/06/19", Set4, Set4));
             FamousSetList.Add(Tuple.Create("2017/06/19", Set5, Set5));

             int expected = 3;

             //Act
             List<string> list = FamousSetList.Select(x => x.Item3).ToList();
             List<string> decList = list.OrderByDescending(x => int.Parse(x.Substring(x.LastIndexOf("=") + 1))).ToList();
        
             List<string> actual = decList.Distinct().Take(5).ToList();
            // List<string> actual = SetFamousRepeats.GetFamousSetRepeat(4, 5);

             //Assert

             Assert.AreEqual(expected, actual.Count());


         }
    }
}
